import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavigationComponent } from './navigation/navigation.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { LoginComponent } from './pages/login/login.component';
import { SelectSandwichComponent } from './pages/select-sandwich/select-sandwich.component';
import { ConfirmSelectionComponent } from './pages/select-sandwich/confirm-selection/confirm-selection.component';
import {AppMaterialModule} from './app-material.module';
import { OrderHistoryComponent } from './pages/order-history/order-history.component';
import {ConfirmationDialogComponent} from './components/dialogs/confirmation-dialog/confirmation-dialog.component';
import { AutoOrderComponent } from './pages/auto-order/auto-order.component';
import { OrderAgainComponent } from './pages/order-history/order-again/order-again.component';

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    LoginComponent,
    SelectSandwichComponent,
    ConfirmSelectionComponent,
    OrderHistoryComponent,
    ConfirmationDialogComponent,
    AutoOrderComponent,
    OrderAgainComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    LayoutModule,
    AppMaterialModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
