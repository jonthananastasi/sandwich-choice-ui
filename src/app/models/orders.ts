class Order {
  id: number;
  sandwich: string;
  img: string;
  date: string;
}
export const ORDERS: Order[] = [
  { id: 1, sandwich: 'Turkey on Wheat with Lettuce, Tomato and Onion', img: './assets/img/sandwiches/sandwich-example.png', date: '12/01/2020'},
  { id: 2, sandwich: 'Turkey on Wheat with Lettuce, Tomato and Onion', img: './assets/img/sandwiches/sandwich-example-2.png', date: '11/01/2020'},
  { id: 3, sandwich: 'Ham on Wheat with Lettuce, Tomato and Onion', img: './assets/img/sandwiches/sandwich-example-3.png', date: '08/01/2020'},
  { id: 4, sandwich: 'Turkey on Wheat with Lettuce, Tomato and Onion', img: './assets/img/sandwiches/sandwich-example.png', date: '07/01/2020'},
  { id: 5, sandwich: 'Beef on Rye with Lettuce, Tomato and Pickle', img: './assets/img/sandwiches/sandwich-example.png', date: '06/01/2020'},
  { id: 6, sandwich: 'Turkey on Wheat with Lettuce, Tomato and Onion', img: './assets/img/sandwiches/sandwich-example.png', date: '05/01/2020'},
  { id: 7, sandwich: 'Turkey on Wheat with Lettuce, Tomato and Onion', img: './assets/img/sandwiches/sandwich-example.png', date: '04/01/2020'},
];
