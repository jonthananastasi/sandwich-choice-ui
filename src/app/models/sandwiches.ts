class Sandwich {
  id: number;
  name: string;
  img: string;
}
export const SANDWICHES: Sandwich[] = [
  { id: 1, name: 'Turkey on Wheat with Lettuce, Tomato and Onion', img: './assets/img/sandwiches/sandwich-example.png'},
  { id: 2, name: 'Beef on Wheat with Lettuce and Tomato', img: './assets/img/sandwiches/sandwich-example-2.png'},
  { id: 3, name: 'Beef on Rye with Lettuce, Tomato and Pickle', img: './assets/img/sandwiches/sandwich-example-3.png'},
  { id: 4, name: 'Ham and Pickle on White bread', img: './assets/img/sandwiches/sandwich-example-4.png'},
  { id: 5, name: '"Onion Lover"  - Ham or beef on Rye with Onion', img: './assets/img/sandwiches/sandwich-example-4.png'},
  { id: 6, name: 'Jamon, jamon - Ham on Wheat with Lettuce and Tomato', img: './assets/img/sandwiches/sandwich-example-3.png'},
  { id: 7, name: 'Turkey Lettuce and Tomato on Rye', img: './assets/img/sandwiches/sandwich-example-2.png'},
  { id: 8, name: 'Beef, Lettuce and Tomato on Rye', img: './assets/img/sandwiches/sandwich-example.png'},
  { id: 9, name: 'Ham on Wheat with Lettuce, Tomato and Onion', img: './assets/img/sandwiches/sandwich-example-2.png'},
];
