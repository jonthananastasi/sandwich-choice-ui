import { Component, OnInit } from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {ORDERS} from '../../models/orders';
import {OrderAgainComponent} from './order-again/order-again.component';

@Component({
  selector: 'app-order-history',
  templateUrl: './order-history.component.html',
  styleUrls: ['./order-history.component.scss']
})
export class OrderHistoryComponent implements OnInit {
  orders = ORDERS;
  constructor(public dialog: MatDialog) { }
  ngOnInit(): void {
  }
  /**
   * Re Order Sandwich
   */
  reorder(order): void {
    const dialogRef = this.dialog.open(OrderAgainComponent, {
      width: '100%',
      height: '100%',
      disableClose: true,
      data: {order},
      panelClass: ['animate__animated', 'animate__slideInRight', 'panelSize'],
      position: {
        top: '0px',
        right: '0px'
      }
    });
    const subscription = dialogRef.afterClosed().subscribe(() => {
      subscription.unsubscribe();
    });
  }
}
