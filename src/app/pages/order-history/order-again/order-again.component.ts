import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog} from '@angular/material/dialog';
import {Router} from '@angular/router';

@Component({
  selector: 'app-order-again',
  templateUrl: './order-again.component.html',
  styleUrls: ['./order-again.component.scss']
})
export class OrderAgainComponent implements OnInit {
  orderOverview = true;
  orderConfirmed = false;
  sendingOrder = false;
  constructor(public dialog: MatDialog, private router: Router, @Inject(MAT_DIALOG_DATA) public data) { }
  ngOnInit(): void {}
  /**
   * Close dialog
   */
  cancelOrder() {
    document.getElementsByClassName('animate__animated')[0].classList.remove('animate__slideInLeft');
    document.getElementsByClassName('animate__animated')[0].classList.add('animate__slideOutRight');
    setTimeout(() => {
      this.dialog.closeAll();
    }, 1000);
  }
  /**
   * Confirm Order
   */
  confirmOrder() {
    this.orderOverview = false;
    this.sendingOrder = true;
    setTimeout(() => {
      this.sendingOrder = false;
      this.orderConfirmed = true;
    }, 2000);
  }
  /**
   * Finish Order
   */
  finishOrder() {
    document.getElementsByClassName('animate__animated')[0].classList.remove('animate__slideInLeft');
    document.getElementsByClassName('animate__animated')[0].classList.add('animate__slideOutRight');
    setTimeout(() => {
      this.dialog.closeAll();
      this.router.navigate(['order-history']);
    }, 1000);
  }
}
