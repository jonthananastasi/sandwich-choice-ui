import { Component, OnInit } from '@angular/core';
import {SANDWICHES} from '../../models/sandwiches';
import {ConfirmationDialogComponent} from '../../components/dialogs/confirmation-dialog/confirmation-dialog.component';
import {MatDialog} from '@angular/material/dialog';

@Component({
  selector: 'app-auto-order',
  templateUrl: './auto-order.component.html',
  styleUrls: ['./auto-order.component.scss']
})
export class AutoOrderComponent implements OnInit {
  sandwiches = SANDWICHES;
  constructor(public dialog: MatDialog) { }
  ngOnInit(): void {
  }
  setAutoOrder() {
    const dialogConfig = {
      autofocus: false,
      data: {
        title: `Confirm auto order?`,
        text: 'Auto order will be enabled and your orders will have placed automatically?',
        continueButtonText: 'Yes',
        cancelButtonText: 'Cancel'
      }
    };
    const confirm = this.dialog.open(ConfirmationDialogComponent, dialogConfig);
    const sub = confirm.afterClosed().subscribe((response) => {
      if (response === 'continue') {
      }
      sub.unsubscribe();
    });
  }
}
