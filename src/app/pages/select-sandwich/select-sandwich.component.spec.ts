import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectSandwichComponent } from './select-sandwich.component';

describe('SelectSandwichComponent', () => {
  let component: SelectSandwichComponent;
  let fixture: ComponentFixture<SelectSandwichComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectSandwichComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectSandwichComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
