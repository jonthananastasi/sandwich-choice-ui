import { Component, OnInit } from '@angular/core';
import {SANDWICHES} from '../../models/sandwiches';
import {MatDialog} from '@angular/material/dialog';
import {ConfirmSelectionComponent} from './confirm-selection/confirm-selection.component';

@Component({
  selector: 'app-select-sandwich',
  templateUrl: './select-sandwich.component.html',
  styleUrls: ['./select-sandwich.component.scss']
})
export class SelectSandwichComponent implements OnInit {
  sandwiches = SANDWICHES;
  constructor(public dialog: MatDialog) { }
  ngOnInit(): void {
  }
  /**
   * Place order
   */
  placeOrder(sandwich): void {
    const dialogRef = this.dialog.open(ConfirmSelectionComponent, {
      width: '100%',
      height: '100%',
      data: {sandwich},
      disableClose: true,
      panelClass: ['animate__animated', 'animate__slideInRight', 'panelSize'],
      position: {
        top: '0px',
        right: '0px'
      }
    });
    const subscription = dialogRef.afterClosed().subscribe(() => {
      subscription.unsubscribe();
    });
  }
}
