import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import {ConfirmationDialogComponent} from '../components/dialogs/confirmation-dialog/confirmation-dialog.component';
import {MatDialog} from '@angular/material/dialog';
import {Router} from '@angular/router';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent {
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );
  constructor(private breakpointObserver: BreakpointObserver, public dialog: MatDialog, private router: Router) {}
  /**
   * Sign out of system
   */
  logout() {
    const dialogConfig = {
      autofocus: false,
      data: {
        title: `Log out?`,
        text: 'Are you sure you want to log out?',
        continueButtonText: 'Yes',
        cancelButtonText: 'Cancel'
      }
    };
    const confirm = this.dialog.open(ConfirmationDialogComponent, dialogConfig);
    const sub = confirm.afterClosed().subscribe((response) => {
      if (response === 'continue') {
        this.router.navigate(['login']);
      }
      sub.unsubscribe();
    });
  }
}
