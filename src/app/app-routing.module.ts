import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from './pages/login/login.component';
import {NavigationComponent} from './navigation/navigation.component';
import {SelectSandwichComponent} from './pages/select-sandwich/select-sandwich.component';
import {OrderHistoryComponent} from './pages/order-history/order-history.component';
import {AutoOrderComponent} from './pages/auto-order/auto-order.component';


const routes: Routes = [
  {path: '', component: LoginComponent, pathMatch: 'full'},
  {
    path: '',
    component: NavigationComponent,
    children: [
      {
        path: 'select-sandwich', component: SelectSandwichComponent,
      },
      {
        path: 'order-history', component: OrderHistoryComponent,
      },
      {
        path: 'auto-order', component: AutoOrderComponent,
      },
    ]
  },
  // Handle all other routes
  {path: '**', redirectTo: ''}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
